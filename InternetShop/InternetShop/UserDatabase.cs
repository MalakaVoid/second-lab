﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetShop
{
    public static class UserDatabase
    {
        private static List<User> users = new List<User>();

        public static void createFirstAdmin()
        {
            users.Add(new Administrator("Adm", "123"));
        }

        public static User GetUser(string username, string passwd)
        {
            Console.WriteLine("UserDatabase: GetUser()");
            foreach (User user in users)
            {
                if (user.username == username && user.passwd == passwd)
                {
                    return user;
                }
            }
            return null;
        }

        public static void AddUser(string username, string passwd, string role)
        {
            Console.WriteLine("UserDatabase: AddUser()");
            if (role == "ADMINISTRATOR")
            {
                users.Add(new Administrator(username, passwd));
            }
            else
            {
                users.Add(new DefaultUser(username, passwd));
            }
        }
    }
}
