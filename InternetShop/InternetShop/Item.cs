﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetShop
{
    public class Item : IObservable
    {
        public string name;
        public double price;
        public string description;
        public List<IObserver> observers;
        public Item(string name, double price, string description)
        {
            this.observers = new List<IObserver>();
            this.name = name;
            this.price = price;
            this.description = description;
        }
        public void RegisterObserver(IObserver obs)
        {
            Console.WriteLine("Item: RegisterObserver()");
            observers.Add(obs);
        }
        public void RemoveObserver(IObserver obs)
        {
            Console.WriteLine("Item: RemoveObserver()");
            observers.Remove(obs);
        }
        public void NotifyObserversAboutBuying()
        {
            Console.WriteLine("Item: NotifyObserversAboutBuying()");
            foreach (IObserver obs in observers)
            {
                obs.Notify($"Товар {name} бы выкуплен.");
            }
            observers = new List<IObserver>();
        }
        public void NotifyObserversAboutChangeOfPrice(double newprice)
        {
            Console.WriteLine("Item: NotifyObserversAboutChangeOfPrice()");
            this.price = newprice;
            foreach (IObserver obs in observers)
            {
                obs.Notify($"У товара {name} была изменена цена на {newprice} руб.");
            }
        }
    }
}
