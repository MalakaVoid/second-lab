﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetShop
{
    class ProgramManager
    {
        public User currentUser { get; set; }
        public ProgramManager()
        {
            // Инициализация фасада
        }

        // Авторизация пользователя
        public string authorizate(string username, string passwd)
        {
            Console.WriteLine("ProgramManager: authorizate()");
            User user = UserDatabase.GetUser(username, passwd);
            if (user is null)
            {
                return "Ошибка авторизации.";
            }
            else
            {
                currentUser = user;
                return "Авторизация прошла успешно.";
            }

        }
        //Добавление нового пользователя
        public string AddUser(string username, string passwd, string role)
        {
            Console.WriteLine("ProgramManager: AddUser()");
            if (currentUser.getUserRole() == "ADMINISTRATOR")
            {
                UserDatabase.AddUser(username, passwd, role);
                return "Новый пользователь успешно добавлен.";
            }
            else
            {
                return "Нет доступа.";
            }
        }
        //Добавление нового товара
        public string AddItem(string name, double price, string description)
        {
            Console.WriteLine("ProgramManager: AddItem()");
            if (currentUser.getUserRole() == "ADMINISTRATOR")
            {
                ItemDatabase.AddItem(name, price, description);
                return "Товар успешно добавлен";
            }
            else
            {
                return "Нет доступа.";
            }
        }
        //Добавление оповещений о товаре
        public string AddNotifyToItem(string itemName)
        {
            Console.WriteLine("ProgramManager: AddNotifyToItem()");
            return ItemDatabase.AddNotifyToItem(itemName, currentUser);

        }
        public string DeleteNotifyToItem(string itemName)
        {
            Console.WriteLine("ProgramManager: DeleteNotifyToItem()");
            return ItemDatabase.DeleteNotifyToItem(itemName, currentUser);
        }
        public string BuyItem(string itemName)
        {
            Console.WriteLine("ProgramManager: BuyItem()");
            return ItemDatabase.BuyItem(itemName);
        }
        public string ChangeOfPrice(string itemName, double newprice)
        {
            Console.WriteLine("ProgramManager: ChangeOfPrice()");
            if (currentUser.getUserRole() == "ADMINISTRATOR")
            {
                ItemDatabase.ChangeItemPrice(itemName, newprice);
                return "Товар успешно добавлен";
            }
            else
            {
                return "Нет доступа.";
            }
        }

    }
}
