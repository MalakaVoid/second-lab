﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetShop
{
    public static class ItemDatabase
    {
        private static List<Item> items = new List<Item>();

        public static void AddItem(string name, double price, string description)
        {
            Console.WriteLine("ItemDatabase: AddItem()");
            items.Add(new Item(name, price, description));
        }
        public static string ChangeItemPrice(string nameItem, double newprice)
        {
            Console.WriteLine("ItemDatabase: ChangeItemPrice()");
            foreach (Item item in items)
            {
                if (item.name == nameItem)
                {
                    item.NotifyObserversAboutChangeOfPrice(newprice);
                    return "Цена была успешно изменена";
                }
            }
            return "Данный товар не найден";
        }
        public static string BuyItem(string nameItem)
        {
            Console.WriteLine("ItemDatabase: BuyItem()");
            foreach (Item item in items)
            {
                if (item.name == nameItem)
                {
                    item.NotifyObserversAboutBuying();
                    items.Remove(item);
                    return "Покупка прошла успешно";
                }
            }
            return "Данный товар не найден";
        }
        public static string AddNotifyToItem(string nameItem, IObserver user)
        {
            Console.WriteLine("ItemDatabase: AddNotifyToItem()");
            foreach (Item item in items)
            {
                if (item.name == nameItem)
                {
                    item.RegisterObserver(user);
                    return "Вы успешно подписались на обновление данного товара.";
                }
            }
            return "Данный товар не найден";
        }
        public static string DeleteNotifyToItem(string nameItem, IObserver user)
        {
            Console.WriteLine("ItemDatabase: DeleteNotifyToItem()");
            foreach (Item item in items)
            {
                if (item.name == nameItem)
                {
                    item.RemoveObserver(user);
                    return "Вы успешно отписались от обновлений данного товара.";
                }
            }
            return "Данный товар не найден";
        }
    }
}
