﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetShop
{
    public abstract class User : IObserver
    {
        public string username { get; set; }
        public string passwd { get; set; }

        public abstract string getUserRole();

        public void Notify(string message)
        {
            Console.WriteLine("User: Notify()");
            Console.WriteLine($"Сообщение для пользователя {username}:\n {message}");
        }

    }

    // Уточнение пользователя, в качестве пользователя по-умолчанию
    class DefaultUser : User
    {
        public DefaultUser(string username, string passwd)
        {
            this.username = username;
            this.passwd = passwd;
        }

        public override string getUserRole()
        {
            Console.WriteLine("DefaultUser: GetUserRole()");
            return "DEFAULT_USER";
        }
    }

    // Уточнение пользователя, в качестве администратора
    class Administrator : User
    {
        public Administrator(string username, string passwd)
        {
            this.username = username;
            this.passwd = passwd;
        }

        public override string getUserRole()
        {
            Console.WriteLine("Administrator: GetUserRole()");
            return "ADMINISTRATOR";
        }
    }
}
