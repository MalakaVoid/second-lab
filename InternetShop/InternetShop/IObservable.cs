﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetShop
{
    public interface IObservable
    {
        void RegisterObserver(IObserver obs);
        void RemoveObserver(IObserver obs);
        void NotifyObserversAboutBuying();
        void NotifyObserversAboutChangeOfPrice(double newprice);
    }
}
