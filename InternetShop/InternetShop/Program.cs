﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetShop
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Пользователь 1
            string username = "Вася";
            string passwd = "qwerty";
            //Пользователь 2
            string username2 = "Андрей";
            string passwd2 = "123";
            UserDatabase.createFirstAdmin(); //Заглушка для создания первого админа

            Console.WriteLine("-------------------------------------------------");
            ProgramManager prman = new ProgramManager(); // клиент для админа
            Console.WriteLine(prman.authorizate("Adm", "123")); // Авторизация под админом
            Console.WriteLine("-------------------------------------------------");
            Console.WriteLine(prman.AddUser(username, passwd, "DEFAULT_USER"));// Добавление пользователя 1
            Console.WriteLine("-------------------------------------------------");
            Console.WriteLine(prman.AddUser(username2, passwd2, "DEFAULT_USER"));// Добавление пользователя 2
            Console.WriteLine("-------------------------------------------------");
            //Добавление товаров---------------------------
            Console.WriteLine(prman.AddItem("Микроволновка", 3000, "Мощная штука"));
            Console.WriteLine("-------------------------------------------------");
            Console.WriteLine(prman.AddItem("Ноутбук", 15000, "Мощная штука"));
            Console.WriteLine("-------------------------------------------------");
            Console.WriteLine(prman.AddItem("Настольная игра", 1000, "Настолка"));
            Console.WriteLine("-------------------------------------------------");
            //---------------------------------------------
            ProgramManager prman1 = new ProgramManager();//клиент для первого пользователя
            Console.WriteLine(prman1.authorizate(username, passwd)); // Авторизация под первым пользователем
            Console.WriteLine("-------------------------------------------------");
            Console.WriteLine(prman1.AddNotifyToItem("Ноутбук"));//Подписка на оповещения о товаре
            Console.WriteLine("-------------------------------------------------");

            ProgramManager prman2 = new ProgramManager();//клиент для второго пользователя
            Console.WriteLine(prman2.authorizate(username2, passwd2)); // Авторизация под вторым пользователем
            Console.WriteLine("-------------------------------------------------");
            Console.WriteLine(prman2.AddNotifyToItem("Ноутбук"));//Подписка на оповещения о товаре
            Console.WriteLine("-------------------------------------------------");

            Console.WriteLine(prman.ChangeOfPrice("Ноутбук", 1200)); //Изменение цены товара - Ноутбук
            Console.WriteLine("-------------------------------------------------");

            Console.WriteLine(prman1.BuyItem("Ноутбук")); //Первый пользователь покупает Ноутбук
            Console.WriteLine("-------------------------------------------------");
        }

    }
}
